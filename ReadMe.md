# VectorRace
The goal of the game is to get the virtual car as far as possible (cross the most checkpoints in the correct order) within a given number of steps.
To achieve this, the player must create a robot driver that modifies the current velocity vector based on information about the car and the track.
The velocity vector can be adjusted along rows and columns by an integer between -1 and +1 [-1, 0, +1].
The number of sensor readings per step cycle is limited.

## Starting
### Prerequisites
- Python 3.8 (minimum)
- Package manager (e.g., PIP)
- PIL (pillow)
- numpy
- matplotlib
- PyYAML
- (datetime, os, sys)
- 
### Operation
#### Race Simulation
The race simulation is handled by the main.py file, which takes the following command-line parameters:
1. TracksFolder/TF: Folder containing track descriptor YAML files.
2. TrackName/TN: The name of the track descriptor YAML file, including extension (optional).
3. DriversFolder/DF: Folder containing implementations of drivers (descendants of DriverBase).
4. LogsFolder/LF: Destination folder for runtime (.log) and trajectory (.coords) logs.
5. LogType/LT: Types of logs to create: None/Events/Coords/Both.
6. VisualizationType/VT: Type of result visualization: None/Animated/Still.
  
If the folder containing track descriptors has multiple files, you must choose the one to use. This can be done by specifying the TrackName parameter or selecting during runtime.

Example race start command:  
```python main.py TF:Tracks TN:Track_1.yml DF:Drivers LF:Logs LT:None VT:Still```

#### Viewing Races Later
1. Race simulation results can be displayed independently using the visualization.py file, which has the following command-line parameters:
2. TracksFolder/TF: Folder containing track descriptor YAML files.
3. LogsFolder/LF: Folder containing trajectory (Coords) logs.
4. LogName/LN: Name of the coordinate log file to load, including extension (optional).
5. VisualizationType/VT: Type of result visualization: Animated/Still/Continuous/Repeat:X.
   Where Continuous means continuous playback, and Repeat means X-times repeated animation playback.
  
If the folder containing logs has multiple files, you must choose the one to use. This can be done by specifying the LogName parameter or selecting during runtime.

Example visualization command:  
```python visualization.py TF:Tracks LF:Logs LN:track01_20231214_075231.coords VT:Animated```

### Track Description
The structure of the YAML descriptor file:

```yaml
image_file: Tracks\track_1.png
name: Square
down_sample: 5
start:
  from: { X: 270, Y: 450 }
  to: { X: 270, Y: 499 }
  direction: { X: 1, Y: 0 }
splits:
  - split 1:
      from: { X: 540, Y: 450 }
      to: { X: 540, Y: 499 }
  - split 2:
      from: { X: 540, Y: 165 }
      to: { X: 540, Y: 214 }
  - split 3:
      from: { X: 210, Y: 165 }
      to: { X: 210, Y: 214 }
  - split 4:
      from: { X: 150, Y: 340 }
      to: { X: 199, Y: 340 }
```
  
The RGB (3x8-bit) image specified in the track descriptor contains a black road on a white background.
Lines crossing the road:
- Start (green): Start line, defined by endpoints and the required direction of movement.
- Splits (blue): Checkpoints to be crossed, in the specified order.
  
### Drivers
The test drivers must be placed in the folder specified by the ```DriversFolder``` parameter when starting ```main.py```.  
**Multiple cars (drivers) can participate in a race, but they do not interact with each other in any way.**

The driver's decision to modify the car's velocity vector (_think) and the modification of the car's velocity vector (update) are separated because the velocity change must be executed even if the decision runs in a separate thread and is interrupted (timeout).

### End of Race
A race continues until either the prescribed number of movement iterations are completed or all cars become inactive.
This happens if:
- The car crashes into a wall (leaves the road).
- Leaving the track is allowed, but the car leaves the map.
- The car's velocity vector has a length of 0 for a specified number of consecutive steps.
- The driver triggers unhandled exceptions a specified number of times (total, not necessarily consecutive).
  
Races can run in two modes:
- Leaving the track is allowed: The car remains operational off-track but has its speed capped. (Leaving the map results in immediate disqualification.)
- Leaving the track is not allowed: On collision with a wall,
  ** the car's speed drops to 0, and it becomes immobile for a few (random) iterations (restart).**
  
The paths traversed by the cars are stored and can be displayed on the track map using the ```Visualizer``` class.

The race results are determined by ranking cars (drivers) based on performance.  
  
### Performance ranking criteria:
1. Distance traveled (number of checkpoints crossed).
2. Time required to travel the distance.
3. Maximum speed achieved.
  
Additional points can be earned based on ranking by maximum speed and the time at which it was first achieved.

## Task Description
The player's task is to create a program component that determines the relative row and column modification (modifier vector) of the car's velocity vector based on data provided by the available car and track sensors. 
The player must keep in mind that acceleration/deceleration and turning operations result in modifier vectors with different directions and row/column modifications based on the car’s orientation.

The robot driver to be created must be implemented as a custom class inherited from the ```DriverBase``` class, and the necessary calculations must be performed by overriding the  
```def _think(self, car_sensor: SensorBase, track_sensor: SensorBase):```  
inherited method. This method is automatically called by the framework at the appropriate time and place.

The velocity vector modification (the row and column coordinates of the modifier vector) must be stored in the public attribute self.relative_speed_change inherited from the ```DriverBase``` class.
The result of the calculations:
1. the change in the velocity vector
2. the relative displacement of the vector's endpoint
3. row and column coordinates of the modifier vector’s endpoint.
  
The new velocity vector will be the sum of the previous velocity vector and the modifier vector.
  
There is a limited amount of time available to calculate the velocity modification during each step cycle. If the time limit is exceeded, the car will be disqualified from the race.
  
The default value of the modifier vector is ```self.relative_speed_change = (0, 0)```, meaning that in case of a driver decision error, the velocity vector remains unchanged (the car's speed and direction do not change).
  
### Tools Available During Development
#### Data Storage
The ```DriverBase``` parent class's storage public attribute (```self.storage```) is a storage object where named values can be stored between modification steps.  
The storage can be used as follows:
- Read: ```self.storage.get(<value name>, <default value if name does not exist>)```
- Write: ```self.storage.set(<value name>, <new value to store>)```
  
Example usage of storage:
- Reading: ```self.storage.get("first_run", True)```
- Writing: ```self.storage.set("first_run", False)```
  
Programmers experienced in object-oriented programming may also use:
- Instance attributes of the custom driver class for data storage.
- Instance methods and/or other custom classes for structuring the solution.
- Global variables and methods are STRICTLY PROHIBITED!
  
#### Sensors
Two types of sensors can be used to solve the task; the exact types are defined by the specific race task:  
- ```CarSensor``` - Device for querying the car's state:
  - get_iteration() -> int: Returns the current car movement step count.
  - get_position() -> (row, column): Returns the car’s current absolute position.
  - get_speed() -> (row, column): Returns the car’s current velocity.
  - get_speed_vector_length() -> float: Returns the current speed (length of the velocity vector).
  - is_restarting() -> bool: Indicates whether the car is stopped/restarting (penalty for attempting to leave the track).  
- Track sensor:
  - ```SensorBase``` superclass - Methods inherited from this base class are unrestricted in use:
        - get_track_size() -> (row, column): Returns the map size in pixels.
        - get_start_direction() -> (row, column): Returns the required starting direction, i.e., the velocity modification needed for starting.
        - can_scan() -> bool: Indicates whether further sensing is possible (if limited number of sensor readings are allowed).
  - ```PointSensor```: Examines track points to check if a point is on the track:
        - is_point_on_track(point) -> PointOnTrackResult: Determines if a point is on the track (LIMITED).
        - is_point_on_map(point) -> PointOnMapResult: Determines if a point is on the map (LIMITED).
        - ```PathSensorBase```: Derived from the ```PointSensor``` class, examines paths as well as track points:
  - Inherits PointSensor functionalities.
    - is_path_homogeneous(start_point, end_point) -> PathCheckResult: Checks if the start and end points are on the track and if the path between them remains consistent with the starting point (LIMITED).
    - ```LineOfSightSensor```: The examined path is a straight line starting from the car.
    - ```LineSensor```: The examined path is a straight line between any two points.
  
*The specific types of sensors available depend on the particular race task!*
  
#### Sensor Results
The methods of individual sensors return instances of the following classes as results.
  
For track point examination, the result type is:  
- (```PointOnTrackResult```) Public components:
  - coordinate: tuple: The examined coordinate (row, column).
  - is_on_track: bool: Is the coordinate on the track?
- (```PointOnMapResult```) Public components:
  - coordinate: tuple: The examined coordinate (row, column).
  - is_on_map: bool: Is the coordinate on the map?
  
For track path examination, the result type (```PathCheckResult```) has the following public components:
- start_coordinate: tuple: Coordinates of the path's starting point (row, column).
- end_coordinate: tuple: Coordinates of the path's endpoint (row, column).
- path: list of tuples: Coordinates of the path points (row, column) from the starting point to the endpoint.
  The path is generated by the ```PathSensorBase``` class's ```get_path(self, r0, c0, r1, c1)``` method.
- is_start_on_track: bool: Is the starting point on the track?
- is_end_on_track: bool: Is the endpoint on the track?
- last_same_as_start: tuple: Coordinates of the last point on the path that matches the type of the starting point.
- last_same_as_start_index: int: Index of the last point on the path that matches the type of the starting point.
  
### Example Driver
In the Drivers/Examples folder, there are two examples of driver implementations.
While neither solves the task efficiently, they provide a good foundation for developing custom solutions by illustrating the programming tools.

```python
import math

from Model.DriverBase import DriverBase
from Model.Sensors.CarSensor import CarSensor
from Model.Sensors.LineSensor import LineSensor


class TestDriver(DriverBase):

    def _think(self, car_sensor: CarSensor, track_sensor: LineSensor):
        # default inputs
        pos_y, pos_x = car_sensor.get_position()
        speed_y, speed_x = car_sensor.get_speed()
        speed_vector_length = car_sensor.get_speed_vector_length()
        iteration = car_sensor.get_iteration()

        # custom values from the storage to local variables
        is_first_run = self.storage.get("first_run", True)
        total_length = self.storage.get("total_length", 0)
        
        # if car is under restart, no movement will be made
        # only track analysis is recommended
        if car_sensor.is_restarting:
            # can is stopped, can not change speed
            self.relative_speed_change = (0, 0)
            
            # do some measurements and calculations
            
            # then leave the method
            return

        # use local variables
        total_length += math.sqrt(speed_y ** 2 + speed_x ** 2)
        if is_first_run:
            print("Track size: {0}".format(track_sensor.get_track_size()))

        # update custom values in the storage
        self.storage.set("first_run", False)
        self.storage.set("total_length", total_length)

        # set output, default in each call: (0, 0)
        # for now, as a test, continuous acceleration in start direction
        self.relative_speed_change = track_sensor.get_start_direction()

        next_position = (
            pos_y + speed_y + self.relative_speed_change[0],
            pos_x + speed_x + self.relative_speed_change[1]
        )

        # test sensor read limit (sensor returns -1 if read limit reached)
        while track_sensor.can_scan():
            # sensor returns: PointOnTrackResult object
            # PointOnTrackResult.is_on_track: bool - is coordinate on track
            sensor_value = track_sensor.is_point_on_track(next_position)
            print(
                iteration,
                pos_y, pos_x,
                total_length,
                speed_y, speed_x, speed_vector_length,
                sensor_value
            )

            if sensor_value.is_on_track:
                # move forward
                pass
            else:
                # turn
                pass
```

### Tips
- If the driver exhibits unjustified random behavior, suspect neglect or incorrect handling of restarts.