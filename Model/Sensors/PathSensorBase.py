from Model.Sensors.Results.PathCheckResult import PathCheckResult
from Model.Sensors.PointSensor import PointSensor


class PathSensorBase(PointSensor):
    def is_path_homogeneous(self, start_coordinate, end_coordinate):
        if self.can_scan():
            self._scan()
            path = self.get_path(
                        start_coordinate[0], start_coordinate[1],
                        end_coordinate[0], end_coordinate[1]
                    )

            last_same_as_start_index = self._track.check_path(path)

            return PathCheckResult(
                start_coordinate, end_coordinate, path,
                self._track.get_point(start_coordinate) == 0,
                self._track.get_point(end_coordinate) == 0,
                path[last_same_as_start_index],
                last_same_as_start_index
            )
        else:
            return None

    def get_path(self, r0, c0, r1, c1):
        raise NotImplementedError()
